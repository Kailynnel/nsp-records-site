import React from 'react';
import DatePicker from '../date-picker/DatePicker';
import moment from 'moment';

/**
 * @description A very ugly way of editing static data (currently only being used on rallies and witnessing events)
 */
export default class EditableField extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      editing: false,
      value: this.props.obj[this.props.label]
    }
  }

  handleSubmit(event) {
    this.setState({
      editing: false
    })


    if (this.props.type == "number") {
      if (!isNaN(parseInt(this.state.value))) {
        this.props.changes[this.props.label] = this.state.value
        this.props.obj[this.props.label] = this.state.value
      }
    }
    else if (this.props.type == "select" && typeof this.props.options[0] == "object" && typeof this.props.labels == "object" && this.state.value != "") {
      this.state.value = JSON.parse(this.state.value);

      for (let key in this.props.labels) {
        if (this.props.labels[key].change) {
          if (this.props.labels[key].changeId != null && this.props.labels[key].changeId != undefined)
            this.props.changes[this.props.labels[key].changeId] = this.state.value[key]
          else
            this.props.changes[key] = this.state.value[key]
        }

        if (this.props.labels[key].obj) {
          if (this.props.labels[key].objId != null && this.props.labels[key].objId != undefined)
            this.props.obj[this.props.labels[key].objId] = this.state.value[key]
          else
            this.props.obj[key] = this.state.value[key]
        }
      }

      this.state.value = this.state.value.display
    } else if ((this.props.valFilter == undefined || this.props.valFilter == null) && !(this.props.type == "select" && this.state.value == "")) {
      this.props.changes[this.props.label] = this.state.value
      this.props.obj[this.props.label] = this.state.value
    } else if (!(this.props.type=="select" && this.state.value=="")) {
      this.props.changes[this.props.label] = this.props.valFilter(this.state.value);
      this.props.obj[this.props.label] = this.props.valFilter(this.state.value);
    }

    this.props.setState({
      changes: this.props.changes,
      obj: this.props.obj
    })
  }

  startEdit() {
    this.setState({editing:true});
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  handleEdit(event) {
    this.setState({
      value: event.target.value
    })
  }

  handleDateEdit(event) {
    this.setState({
      value: moment(event.target.value).format("YYYY-MM-DD")
    })
  }

  render() {
    if (this.state.editing && this.props.editing && this.props.isAdmin) {
      return (
        <span>
          {
            this.props.type == "select" ?
              <select onChange={this.handleEdit.bind(this)}>
                <option value=""></option>
                {
                  this.props.options.map((obj, index) => {
                    if (typeof obj == "object")
                      return <option value={JSON.stringify(obj)}>{obj.display}</option>
                    else
                      return <option>{obj}</option>
                  })
                }
              </select>
            : this.props.type == "textfield" ?
              <div className="mdl-textfield mdl-js-textfield">
                <textarea onChange={this.handleEdit.bind(this)} className="mdl-textfield__input" type="text" rows= "3" id={`textfield-${this.props.label}`} ></textarea>
                <label className="mdl-textfield__label" htmlFor={`textfield-${this.props.label}`}>{this.props.obj[this.props.label]}</label>
              </div>
            : this.props.type == "date" ?
              <DatePicker
                label="Date"
                inputId={`date-${this.props.label}`}
                onChange={this.handleDateEdit.bind(this)}
                value={moment(this.state.value).format("MM/DD/YYYY")}
              />
            : this.props.type == "number" ?
              <div className="mdl-textfield mdl-js-textfield">
                <input onChange={this.handleEdit.bind(this)} className="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id={`number-${this.props.label}`}/>
                <label className="mdl-textfield__label" htmlFor={`number-${this.props.label}`}>{this.props.obj[this.props.label]}</label>
                <span className="mdl-textfield__error">Input is not a number!</span>
              </div>
            :
              <div className="mdl-textfield mdl-js-textfield">
                <input onChange={this.handleEdit.bind(this)} className="mdl-textfield__input" type="text" id={`text-${this.props.label}`}/>
                <label className="mdl-textfield__label" htmlFor={`text-${this.props.label}`}>{this.props.obj[this.props.label]}</label>
              </div>
          }
          <button onClick={this.handleSubmit.bind(this)} className="mdl-button mdl-js-button mdl-button--icon">
            <i className="material-icons">check_circle</i>
          </button>
        </span>
      )
    } else if (this.props.editing && this.props.isAdmin) {
      return (<span>
        <span className={this.props.cssClass}>
          {
            typeof this.props.displayFilter != "function" ?
              this.props.obj[this.props.label]
            :
              this.props.displayFilter(this.props.obj[this.props.label])
          }
        </span>

        <button onClick={this.startEdit.bind(this)} className="mdl-button mdl-js-button mdl-button--icon">
          <i className="material-icons">edit</i>
        </button>
      </span>)
    } else {
      return (<span className={this.props.cssClass}>
        {
          typeof this.props.displayFilter != "function" ?
            this.props.obj[this.props.label]
          :
            this.props.displayFilter(this.props.obj[this.props.label])
        }
      </span>)
    }
  }

}
