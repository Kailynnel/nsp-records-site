import React from 'react';
import { Bar } from 'react-chartjs-2';
import 'whatwg-fetch';
import analytics from '../../../utils/analytics';

/**
 * @description This is the main graph used on the main dashboard to show all time ministry analytics
 * @todo Make this component way more reusable, maybe a component that takes an array of analytics routes to query and graphically displays the result of the query
 */
export default class MainGraph extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			labels: [

			],
			witnessingData: [

			],
			rallyData: [

			],
			indicatedDecisions: [

			],
			schoolsData: [

			],

			options: {
				responsive: true,
				tooltips: {
					mode: 'label'
				},
				elements: {
					line: {
						fill: false
					}
				},
				scales: {
					xAxes: [
						{
							display: true,
							gridLines: {
								display: false
							},
							labels: []
						}
					],
					yAxes: [
						{
							type: 'linear',
							display: true,
							position: 'left',
							id: 'y-axis-1',
							gridLines: {
								display: false
							},
							labels: {
								show: true
							}
						},
						{
							type: 'linear',
							display: true,
							position: 'right',
							id: 'y-axis-2',
							gridLines: {
								display: false
							},
							labels: {
								show: true
							}
						}
					]
				}
			}
		}
	}

	setLabels() {
		let data = {

		}
		let date = new Date();

		let curYear = date.getFullYear();
		let month = date.getMonth();
		let year = 2004;

		if (month > 7) {
			curYear++;
		}

		while (year < curYear) {
			data[`${year}-${year + 1}`] = {
				rallyData: 0,
				witnessingData: 0,
				indicatedDecisions: 0,
				schoolsData: 0
			}
			year++;
		}

		this.fetchSchoolData(data);
	}

	fetchSchoolData(dataset) {
		const promises = [
			analytics.count('schools FOR EACH year'),
			analytics.count('rallies FOR EACH year'),
			analytics.sum('rallies (indicated_decisions) FOR EACH year'),
			analytics.count('witnessing FOR EACH year'),
			analytics.sum('witnessing (indicated_decisions) FOR EACH year'),

		]
		
		Promise.all(promises).then(results => {
			const [
				schools,
				rallies,
				rallyDecisions,
				witnessing,
				witnessingDecisions
			] = results;

			for (let year in dataset) {
				dataset[year].schoolsData = schools[year];
				dataset[year].rallyData = rallies[year];
				dataset[year].witnessingData = witnessing[year];
				dataset[year].indicatedDecisions = (rallyDecisions[year] || 0) + (witnessingDecisions[year] || 0);
			}

			this.finish(dataset);
		})
	}

	finish(dataset) {
		let labels = Object.keys(dataset);

		let witnessingData = [];
		let rallyData = [];
		let indicatedDecisions = [];
		let schoolsData = [];

		for (var i = 0; i < labels.length; i++) {
			witnessingData.push(dataset[labels[i]].witnessingData);
			rallyData.push(dataset[labels[i]].rallyData);
			indicatedDecisions.push(dataset[labels[i]].indicatedDecisions);
			schoolsData.push(dataset[labels[i]].schoolsData);
		}

		this.setState({
			labels,
			witnessingData,
			rallyData,
			indicatedDecisions,
			schoolsData
		})
	}

	componentDidMount() {
		this.setLabels();
	}

	getCanvasElement() {
		return this.chart.chart_instance.chart.ctx.canvas.toDataURL();
	}

	render() {

		return <div>
			<Bar
				ref={(ref) => this.chart = ref}
				data={{
					datasets: [
						//witnessing events
						{
							backgroundColor: "#674EA7",
							borderColor: "#674EA7",
							pointBorderColor: "#674EA7",
							pointHoverBackgroundColor: "#674EA7",
							pointHoverBorderColor: "#674EA7",
							type: "line",
							label: "Witnessing Events",
							data: this.state.witnessingData,
							yAxisID: "y-axis-2"
						},

						//rallies
						{
							backgroundColor: "#DC3912",
							borderColor: "#DC3912",
							pointBorderColor: "#DC3912",
							pointHoverBackgroundColor: "#DC3912",
							pointHoverBorderColor: "#DC3912",
							type: "line",
							label: "Rallies",
							data: this.state.rallyData,
							yAxisID: "y-axis-2"
						},

						//indicated decisions
						{
							backgroundColor: "#3366CC",
							borderColor: "#3366CC",
							pointBorderColor: "#3366CC",
							pointHoverBackgroundColor: "#3366CC",
							pointHoverBorderColor: "#3366CC",
							type: "line",
							label: "Indicated Decisions",
							data: this.state.indicatedDecisions,
							yAxisID: "y-axis-2"
						},

						//active schools
						{
							backgroundColor: "#109618",
							borderColor: "#109618",
							type: "bar",
							label: "Schools",
							data: this.state.schoolsData,
							yAxisID: "y-axis-1"
						},
					]
				}}
				options={{
					responsive: true,
					tooltips: {
						mode: 'label'
					},
					elements: {
						line: {
							fill: false
						}
					},
					scales: {
						xAxes: [
							{
								display: true,
								gridLines: {
									display: false
								},
								labels: this.state.labels
							}
						],
						yAxes: [
							{
								type: 'linear',
								display: true,
								position: 'left',
								id: 'y-axis-1',
								gridLines: {
									display: false
								},
								labels: {
									show: true
								}
							},
							{
								type: 'linear',
								display: true,
								position: 'right',
								id: 'y-axis-2',
								gridLines: {
									display: false
								},
								labels: {
									show: true
								}
							}
						]
					}
				}}
			/>
		</div>

	}

}
