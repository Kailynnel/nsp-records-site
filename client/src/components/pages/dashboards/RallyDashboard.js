import React, {Component} from 'react';
import {Link,NavLink} from 'react-router-dom';
import 'whatwg-fetch';
import moment from 'moment';
import SkyLight from 'react-skylight'
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';

import Card from '../../elements/card/Card';

import CollapseButton from '../../elements/collapse-button/CollapseButton';
import EditableField from '../../elements/editable-field/EditableField';

import BasicModule from '../../elements/modules/BasicModule';

export default class RallyDashboard extends React.Component {
  constructor(props) {
    super(props);

    let {id} = props.match.params;

    this.state = {
      id,
      obj: null,
      allCollapsed: true,
      nextRally: -1,
      previousRally: -1,
      isLoading: true,
      requestmessage:"",
      submittingRequest:false,
      suggestedEdit: "",
      changes: {},
      isEdit: false,
      isAdmin: false,
      currentImage: 0
    };

    this.quality = [
      "NA",
      "very bad",
      "bad",
      "okay",
      "good",
      "very good"
    ]

    this.impactHeader = [
      {
        display: "Total Attendance",
        data: "total_attendance",
        importance:5,
        isNum: true
      },
      {
        display: "Indicated Decisions",
        data:"indicated_decisions",
        importance:5,
        isNum: true,
      },
      {
        display: "Club Attendance",
        data:"club_attendance",
        importance:3,
        isNum: true,
      },
    ]

    this.programHeader = [
      {
        display: "Week Theme",
        data: "week_theme",
        importance:4,
      },
      {
        display: "Daily Theme",
        data: "daily_theme",
        importance:3,
      },
      {
        display: "Rally Type",
        data: "type",
        importance:3,
      },
      {
        display: "Location",
        data: "location",
        importance:2,
      },
      {
        display: "Time",
        data: "time",
        importance:2
      }
    ]

    this.speakerHeader = [
      {
        display: "Name",
        data: "speaker_name",
        importance:4
      },
      {
        display: "Organization",
        data: "speaker_organization",
        importance:2,
      }
    ]

    this.performerHeader = [
      {
        display: "Name",
        data: "performer_name",
        importance:4
      },
      {
        display: "Stage Name",
        data: "performer_stagename",
        impotance:4
      },
      {
        display: "Organization",
        data: "performer_organization",
        importance:2
      }
    ]

    this.materialsHeader = [
      {
        display: "Gospel Tracts",
        data: "materials_gospel_booklets",
        isNum: true,
      },
      {
        display: "Lifebooks",
        data: "materials_lifebooks",
        isNum: true,
      },
      {
        display: "Gospels of John",
        data: "materials_gospels_of_john",
        isNum: true,
      },
      {
        display: "Bibles",
        data: "materials_bibles",
        isNum: true,
      }
    ]
  }

  fetchRally() {
    fetch(`/api/rallies/${this.state.id}`,{credentials: 'include'}).then(response => response.json()).then(
      rally => {
        this.setState(
          {
            obj:rally[0],
            storyCollapsed: false,
            isLoading:false
          },
          () => {
            this.getUser();
          }
        );
        this.fetchNextRallies();
      }
    );
  }

  getUser() {
    fetch("/api/user",{credentials:"include"}).then(response => {
      return response.json()
    }).then(
      user => {
        this.setState({
          isAdmin: user.isAdmin
        }, () => {
          if (this.state.isAdmin) {
            this.getSchoolNames();
          }
        })
      }
    )
  }

  getSchoolNames() {
    fetch(`/api/admin/getschools/${this.state.obj.date}`, {credentials:'include'}).then(response=> response.json()).then(
      allSchools => {
        this.setState({
          schools: allSchools.map(
            obj => {
              return {
                display: obj.name,
                school_id: obj.id,
                schoolreg_id: obj.registration_id,
                chapter_id: obj.chapter_id,
                chapter_name: obj.chapter_name
              }
            }
          )
        })
      }
    )
  }

  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }

  fetchNextRallies() {
    fetch(`/api/rallies/lite/school_id/${this.state.obj.school_id}`,{credentials: 'include'}).then(response => {
      return response.json()
    }).then (
      allRallies => {
        let nextRally = -1;
        let previousRally = -1;

        for (var i = 0; i < allRallies.length; i++) {
          if(allRallies[i].id == this.state.id) {
            if (i != 0) {
              previousRally = allRallies[i-1].id;
            }
            if (i != allRallies.length-1) {
              nextRally = allRallies[i+1].id;
            }
          }
        }

        this.setState({
          previousRally,
          nextRally,
          obj:this.state.obj
        });
      }
    );
  }

  requestContactInfo() {
    this.setState({
      submittingRequest:true
    });
    fetch(`/api/contactinfo/${this.state.id}`, {credentials:'include'}).then(response => {
      return response.json()
    }).then(
      response => {
        if (response.error) {
          this.setState({
            requestmessage:"Your request failed to send, feel free to email IT directly to retreive the desired information.",
            submittingRequest:false,
          })
          this.modal.show();
        } else {
          this.setState({
            requestmessage:"Your request was successfully submitted. It may take a while to process. You will be emailed shortly.",
            submittingRequest:false,
          })
          this.modal.show();
        }
      }
    )
  }

  componentDidMount() {
    this.fetchRally();
  }

  handleCollapseAllState() {
    this.setState( {
      allCollapsed:!this.state.allCollapsed
    })
  }

  setPrevRally() {
    this.setState(
      {
        id:this.state.previousRally,
        isLoading:true,
      }
    )

    this.fetchRally();
  }

  setNextRally() {
    this.setState(
      {
        id:this.state.nextRally,
        isLoading:true,
      }
    )

    this.fetchRally();
  }

  textFieldChange(event) {
    this.setState({
      suggestedEdit:event.target.value
    })
  }

  submitSuggestedEdit() {
    this.requestEditModal.hide();

    fetch(`/api/suggestedit/rally/${this.state.id}/${this.state.suggestedEdit}`, {credentials:'include'}).then(response => {
      return response.json()
    }).then(
      response => {
        if (response.error) {
          this.setState({
            requestmessage:"Your request failed to send, feel free to email IT directly to perform the desired action.",
            submittingRequest:false,
          })
          this.modal.show();
        } else {
          this.setState({
            requestmessage:"Your request was successfully submitted. It may take a while to process.",
            submittingRequest:false,
          })
          this.modal.show();
        }
      }
    )
  }

  postChanges() {
    fetch(`/api/admin/rally/${this.state.id}`, {credentials:"include",method:"POST",headers: {
        'Content-Type':'application/json'
      },body:JSON.stringify({changes:this.state.changes})}).then(
      (response) => response.json()
    ).then(
      (response) => {
        if (response.status == 200) {
          this.setState({
            requestmessage:"Your change was made!",
          })
          this.modal.show();
        } else if (response.status == 401) {
          this.setState({
            requestmessage:"Impostor!! You are no admin!!",
          })
          this.modal.show();
        } else if (response.status == 500) {
          this.setState({
            requestmessage:"My sincerest apologies, it appears as if the server made an oopsies..."
          })
          this.modal.show();
        }
        this.setState({
          changes: {}
        })
      }
    )
  }

  getContactInfo() {
    fetch(`/api/admin/contactinfo/${this.state.id}`,{credentials:"include"}).then(response => response.json()).then(
      (contact_info) => {
        if (contact_info.status) {
          return;
        }
        contact_info = contact_info[0];

        this.state.obj.speaker_email = contact_info.speaker_email
        this.state.obj.speaker_phone = contact_info.speaker_phone
        this.state.obj.performer_email = contact_info.performer_email
        this.state.obj.performer_phone = contact_info.performer_phone
        this.setState({
          obj: this.state.obj
        })
      }
    )
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  handleEdit() {
    if (!this.state.isAdmin) {
      this.requestEditModal.show()
      return;
    } else if (!this.state.isEdit) {
      this.setState({
        isEdit: true
      })
    } else {
      this.postChanges();
      this.setState({
        isEdit: false,
        changes: {}
      })
    }
  }

  cancelEdit() {
    this.setState({
      isEdit: false,
      changes: {}
    })
  }

  render() {
    const cardContent = {
      "padding": "8px",
      "paddingTop": "0"
    }

    let photos = []
    if (this.state.obj != null && typeof this.state.obj.picture_url == "string") {
      photos = this.state.obj.picture_url.split(",").map(
        url => {
          if (!url.includes("http://") && !url.includes("https://"))
            return {
              src: `https://drive.google.com/uc?export=view&id=${url}`,
              width: 1,
              height: 1
            }
          else
            return {
              src: url,
              width: 1,
              height: 1
            }
        }
      )
    }

    if (this.state.obj != null && !this.state.isLoading) {
      return (
        <div>
          <Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">

            <h3 className="mdl-layout-title mdl-cell mdl-cell--1-col">Rally Details (#{this.state.id})</h3>
            <div className="mdl-layout-spacer"></div>
            <h3 className="mdl-layout-title mdl-cell mdl-cell--4-col">
            {
              !this.state.isEdit ?
                <Link to={`/school/${this.state.obj.school_id}`}>{this.state.obj.school_name}</Link>
              :
                <EditableField
                  obj={this.state.obj}
                  label="school_name"
                  labels={{
                    display: {
                      obj: true,
                      objId: "school_name"
                    },
                    schoolreg_id: {
                      change: true,
                    },
                    school_id: {
                      obj: true
                    },
                    chapter_id: {
                      obj: true
                    },
                    chapter_name: {
                      obj: true
                    }
                  }}
                  type="select"
                  options={this.state.schools}
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />
            }
            {" "}(<Link to={`/chapter/custom?ids=${this.state.obj.chapter_id}&query_title=${this.state.obj.chapter_name}`}>{this.state.obj.chapter_name}</Link>)</h3>

            <span className="mdl-cell mdl-cell--2-col">
              <NavLink
                to={
                  `/rally/${this.state.previousRally}`
                }
              >
                <button className = "mdl-button mdl-js-button" disabled={this.state.previousRally == -1} onClick={this.setPrevRally.bind(this)}>
                  <i className = "material-icons">arrow_back</i> Previous Rally
                </button>
              </NavLink>
            </span>

            <span className="mdl-cell mdl-cell--2-col">
              <NavLink
                to={
                  `/rally/${this.state.nextRally}`
                }
              >
                <button className = "mdl-button mdl-js-button" disabled={this.state.nextRally == -1} onClick={this.setNextRally.bind(this)}>
                  Next Rally <i className = "material-icons">arrow_forward</i>
                </button>
              </NavLink>
            </span>
            {
              !this.state.isEdit ?
                <h3 className="mdl-layout-title mdl-cell mdl-cell--2-col">{moment(this.state.obj.date.substring(0,10)).format('MM/DD/YYYY')}</h3>
              :
                <EditableField
                  obj={this.state.obj}
                  label="date"
                  type="date"
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />
            }

          </Card>

          <div className="mdl-grid">
            <div className="mdl-cell--10-col mdl-cell">
              <button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.handleEdit.bind(this)}>
                {
                  !this.state.isAdmin ?
                    "Request Edit"
                  : this.state.isEdit ?
                    "Submit Changes"
                  :
                    "Edit"
                }
              </button>
              {
                this.state.isEdit ?
                  <button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.cancelEdit.bind(this)}>
                    Cancel Edit
                  </button>
                :
                  ""
              }
            </div>

            <div className="mdl-cell--2-col mdl-cell">
              <CollapseButton
                collapsed={this.state.allCollapsed}
                collapsed_label="Show All"
                onClick={this.handleCollapseAllState.bind(this)}
                raised={true}
                uncollapsed_label="Hide All"
              />
            </div>
          </div>

          <BasicModule
            showing={this.state.obj.stories != "" && this.state.obj.stories != null}
            obj={this.state.obj}
            headerElements = {[]}
            title="Story"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <EditableField
              obj={this.state.obj}
              label="stories"
              type="textfield"
              changes={this.state.changes}
              setState={this.setState.bind(this)}
              editing={this.state.isEdit}
              isAdmin={this.state.isAdmin}
            />
          </BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements = {this.impactHeader}
            title="Impact"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          ></BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements = {this.programHeader}
            title="Program"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              <li>
                    <p>Contact cards{" "}
                      <EditableField
                        obj={this.state.obj}
                        label="was_contact_cards"
                        type="select"
                        options={[
                          "were",
                          "were not"
                        ]}
                        displayFilter= {
                          (value) => {
                            if (value == "Yes") {
                              return <span className="positive">were</span>
                            } else {
                              return <span className="negative">were not</span>
                            }
                          }
                        }
                        valFilter= {
                          (value) => {
                            if (value == "were") {
                              return "Yes"
                            } else {
                              return "No"
                            }
                          }
                        }
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      /> explained {
                        this.state.obj.was_contact_cards == "Yes" ?
                          <span> by{" "}
                              <EditableField
                                obj={this.state.obj}
                                label="card_explainedby"
                                type="text"
                                changes={this.state.changes}
                                setState={this.setState.bind(this)}
                                editing={this.state.isEdit}
                                isAdmin={this.state.isAdmin}
                              /> and {
                                this.state.isEdit ?
                                  <span>they did{" "}<EditableField
                                    obj={this.state.obj}
                                    label="card_explained"
                                    type="number"
                                    changes={this.state.changes}
                                    setState={this.setState.bind(this)}
                                    editing={this.state.isEdit}
                                    isAdmin={this.state.isAdmin}
                                  />
                                  /5
                                  </span>
                                : this.state.obj.card_explained == 0 ?
                                  <span>we dont know how they did.</span>
                                :
                                  <span>they did a <span className="undl">{this.quality[this.state.obj.card_explained]}</span> job ({this.state.obj.card_explained}/5)</span>
                          } </span>
                        : ""
                      }
                      </p>
              </li>
              <li>
                <p>Data was collected by <EditableField
                  obj={this.state.obj}
                  label="collected_data"
                  type="text"
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />
                </p>
              </li>
              <li>
                <p>There were <span className="emphasis"><EditableField
                  obj={this.state.obj}
                  label="students_participating"
                  type="number"
                  displayFilter= {
                    (val) => {
                      if (val == "")
                        return "NaN"
                      else
                        return val
                    }
                  }
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />
                </span> high school students participating, (<EditableField
                  obj={this.state.obj}
                  label="student_names_participating"
                  type="textfield"
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />)
                </p>
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing={this.state.obj.was_materials_distributed == "Yes"}
            obj={this.state.obj}
            headerElements = {this.materialsHeader}
            title="Materials"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              <li>
                <p>There <EditableField
                  obj={this.state.obj}
                  label="was_materials_table"
                  type="select"
                  options={[
                    "was",
                    "was not"
                  ]}
                  displayFilter={
                    (val) => {
                      if (val == "Yes") {
                        return <span className="positive">was</span>
                      } else {
                        return <span className="negative">was not</span>
                      }
                    }
                  }
                  valFilter={
                    (val) => {
                      if (val == "was") {
                        return "Yes"
                      } else {
                        return "No"
                      }
                    }
                  }
                  changes={this.state.changes}
                  setState={this.setState.bind(this)}
                  editing={this.state.isEdit}
                  isAdmin={this.state.isAdmin}
                />{" "}
                a materials table</p>
              </li>
              {
                 this.state.isEdit ?
                  <li><p>
                    <EditableField
                      obj={this.state.obj}
                      label="materials_other"
                      type="number"
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    /> other materials were distributed (<EditableField
                      obj={this.state.obj}
                      label="materials_other_type"
                      type="text"
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    />)</p>
                  </li>
                 : this.state.obj.materials_other > 0 && this.state.obj.materials_other_type != "" ?
                  <li>
                    <p>{this.state.obj.materials_other} other materials were distributed ({this.state.obj.materials_other_type})</p>
                  </li>
                  : <li><p>No other materials were distributed</p></li>
              }
            </ul>
          </BasicModule>

          <BasicModule
            showing = {this.state.obj.was_speaker == "Yes"}
            obj = {this.state.obj}
            headerElements = {this.speakerHeader}
            title = "Speaker"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              {
                this.state.isEdit ?
                  <li>
                    <p>Was speaker: <EditableField
                      obj={this.state.obj}
                      label="was_speaker"
                      type="select"
                      options={[
                        "Yes",
                        "No"
                      ]}
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    /></p>
                  </li>
                : ""
              }
              {
                this.state.isAdmin ?
                  <li>
                    Email: <EditableField
                      obj={this.state.obj}
                      label="speaker_email"
                      type="text"
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    /> - Phone: <EditableField
                      obj={this.state.obj}
                      label="speaker_phone"
                      type="text"
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    />
                  </li>
                : this.state.obj.speaker_no_contact == 0 ?
                  <li>
                    {
                      !this.state.submittingRequest ?
                        <p><a onClick={this.requestContactInfo.bind(this)}>Request speaker contact info</a></p>
                      :
                        <p>Submitting request...</p>
                    }
                  </li>
                :
                  <li>
                    <p>This speaker has no stored contact information</p>
                  </li>
              }
              {
                this.state.isEdit ?
                  <li>
                    <p>Speaker was: <EditableField
                        obj={this.state.obj}
                        label="speaker_student_guest"
                        type="select"
                        options={[
                          "Student",
                          "Guest"
                        ]}
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                    </p>
                  </li>
                : this.state.obj.speaker_student_guest != "" && typeof this.state.obj.speaker_student_gues == "string" ?
                  <li>
                    <p>This speaker was a <span className="emphasis">{this.state.obj.speaker_student_guest.toLowerCase()}</span></p>
                  </li>
                : ""
              }
              {
                this.state.isEdit ?
                  <li>
                    <p>Speaker engagement: <EditableField
                        obj={this.state.obj}
                        label="speaker_engagement"
                        type="select"
                        options={[
                          "0",
                          "1",
                          "2",
                          "3",
                          "4",
                          "5",
                        ]}
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                      </p>
                  </li>
                : this.state.obj.speaker_engagement != 0 ?
                  <li><p>This speakers engagement level was described as <span className="undl">{this.quality[this.state.obj.speaker_engagement]}</span> ({this.state.obj.speaker_engagement}/5)</p></li>
                : <li><p>This speakers engagement level was not recorded</p></li>
              }
              <li>
                {
                  this.state.isEdit ?
                    <p>Speaker topically known: <EditableField
                        obj={this.state.obj}
                        label="speaker_topically_known"
                        type="select"
                        options={[
                          "Yes",
                          "No"
                        ]}
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      /> ({
                        this.state.obj.speaker_topically_known == "Yes" ?
                          <EditableField
                            obj={this.state.obj}
                            label="speaker_topic"
                            type="text"
                            changes={this.state.changes}
                            setState={this.setState.bind(this)}
                            editing={this.state.isEdit}
                            isAdmin={this.state.isAdmin}
                          />
                        : <span className="emphasis">NO TOPIC</span>
                      })
                    </p>
                  : this.state.obj.speaker_topically_known == "Yes" ?
                    <p>This speaker is known for speaking on the topic of <span className="emphasis">{this.state.obj.speaker_topic}</span></p>
                  : <p>This speaker is not known for speaking on any particular topic</p>
                }
              </li>
              <li>
                {
                  this.state.isEdit ?
                    <p>This speaker{" "}
                      <EditableField
                        obj={this.state.obj}
                        label="speaker_gospel_shared"
                        type="select"
                        options={[
                          "did",
                          "did not"
                        ]}
                        displayFilter={
                          (val) => {
                            if (val=="Yes") {
                              return <span className="positive">did</span>
                            } else {
                              return <span className="negative">did not</span>
                            }
                          }
                        }
                        valFilter={
                          (val) => {
                            if (val == "did") {
                              return "Yes"
                            } else {
                              return "No"
                            }
                          }
                        }
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      /> share the gospel {
                        this.state.obj.speaker_gospel_shared == "Yes" ?
                          <span>and <EditableField
                              obj={this.state.obj}
                              label="speaker_invitation"
                              type="select"
                              options={[
                                "did",
                                "did not"
                              ]}
                              displayFilter={
                                (val) => {
                                  if (val == "Yes") {
                                    return <span className="positive">did</span>
                                  } else {
                                    return <span className="negative">did not</span>
                                  }
                                }
                              }
                              valFilter={
                                (val) => {
                                  if (val == "did") {
                                    return "Yes"
                                  } else {
                                    return "No"
                                  }
                                }
                              }
                              changes={this.state.changes}
                              setState={this.setState.bind(this)}
                              editing={this.state.isEdit}
                              isAdmin={this.state.isAdmin}
                            /> offer a <EditableField
                                obj={this.state.obj}
                                label="speaker_physical_invitation"
                                type="select"
                                options={[
                                  "physical",
                                  "non physical"
                                ]}
                                displayFilter={
                                  (val) => {
                                    if (val == "Yes") {
                                      return <span className="emphasis">physical</span>
                                    } else {
                                      return <span className="emphasis">non physical</span>
                                    }
                                  }
                                }
                                valFilter={
                                  (val) => {
                                    if (val == "physical") {
                                      return "Yes"
                                    } else {
                                      return "No"
                                    }
                                  }
                                }
                                changes={this.state.changes}
                                setState={this.setState.bind(this)}
                                editing={this.state.isEdit}
                                isAdmin={this.state.isAdmin}
                              /> invitation to recieve Christ
                          </span>
                        : ""
                      } {
                        this.state.obj.speaker_gospel_shared == "Yes" ?
                          <span>The quality of their gospel presentation was <EditableField
                              obj={this.state.obj}
                              label="speaker_gospel_clear"
                              type="select"
                              options={[
                                "0",
                                "1",
                                "2",
                                "3",
                                "4",
                                "5"
                              ]}
                              changes={this.state.changes}
                              setState={this.setState.bind(this)}
                              editing={this.state.isEdit}
                              isAdmin={this.state.isAdmin}
                            /> /5</span>
                          : ""
                      }
                    </p>
                  : this.state.obj.speaker_gospel_shared == "Yes" ?
                    <p>This speaker <span className="positive">did</span> share the gospel {
                      this.state.obj.speaker_invitation == "Yes" || this.state.obj.speaker_physical_invitation == "Yes" ?
                        <span>and <span className = "positive">did</span> offer a{this.state.obj.speaker_physical_invitation == "Yes" ? <span className="undl"> physical</span> : <span>n</span>} invitation to receive the gospel.</span>
                      :
                        <span>but <span className = "negative">did not</span> offer an invitation to receive the gospel</span>
                    } {
                      this.state.obj.speaker_gospel_clear != 0 ?
                        <span>They did a <span className="undl">{this.quality[this.state.obj.speaker_gospel_clear]}</span> job at explaining the gospel ({this.state.obj.speaker_gospel_clear}/5)</span>
                        :""
                    }</p>
                  :
                    <p>This speaker <span className="negative">did not</span> share the gospel at this rally</p>
                }
              </li>
              <li>
                {
                  this.state.isEdit ?
                    <p>
                      This speaker <EditableField
                          obj={this.state.obj}
                          label="speaker_explained_saved"
                          type="select"
                          options={[
                            "did",
                            "did not"
                          ]}
                          valFilter={
                            (val) => {
                              if (val=="did") {
                                return "Yes"
                              } else {
                                return "No"
                              }
                            }
                          }
                          displayFilter={
                            (val) => {
                              if (val=="Yes") {
                                return <span className="positive">did</span>
                              } else {
                                return <span className="negative">did not</span>
                              }
                            }
                          }
                          changes={this.state.changes}
                          setState={this.setState.bind(this)}
                          editing={this.state.isEdit}
                          isAdmin={this.state.isAdmin}
                        /> explain salvation {
                          this.state.obj.speaker_explained_saved == "Yes" ?
                            <span>and they were rated <EditableField
                                obj={this.state.obj}
                                label="speaker_explained_saved_clear"
                                type="select"
                                options={[
                                  "0",
                                  "1",
                                  "2",
                                  "3",
                                  "4",
                                  "5"
                                ]}
                                changes={this.state.changes}
                                setState={this.setState.bind(this)}
                                editing={this.state.isEdit}
                                isAdmin={this.state.isAdmin}
                              /> /5</span>
                          :
                           ""
                        }
                    </p>
                  : this.state.obj.speaker_explained_saved == "Yes" ?
                    <p>This speaker <span className = "positive">did</span> explain salvation and {
                      this.state.obj.speaker_explained_saved_clear != 0 ?
                        <span>they did a <span className="undl">{this.quality[this.state.obj.speaker_explained_saved_clear]}</span> job ({this.state.obj.speaker_explained_saved_clear}/5)</span>
                      : <span>it is unknown how clearly they did so</span>
                    }</p>
                  :
                    <p>This speaker <span className = "negative">did not</span> explain salvation</p>
                }
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing= {this.state.obj.was_performer == "Yes"}
            obj = {this.state.obj}
            headerElements = {this.performerHeader}
            title = "Performer"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              {
                this.state.isEdit ?
                  <li>
                    <p>Was performer: <EditableField
                          obj={this.state.obj}
                          label="was_performer"
                          type="select"
                          options={[
                            "Yes",
                            "No"
                          ]}
                          changes={this.state.changes}
                          setState={this.setState.bind(this)}
                          editing={this.state.isEdit}
                          isAdmin={this.state.isAdmin}
                        />
                      </p>
                  </li>
                : ""
              }
              {
                  this.state.isAdmin ?
                    <li>
                      Email: <EditableField
                        obj={this.state.obj}
                        label="performer_email"
                        type="text"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      /> - Phone: <EditableField
                        obj={this.state.obj}
                        label="performer_phone"
                        type="text"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                    </li>
                 : this.state.obj.performer_no_contact == 0 ?
                    <li>
                      {
                        !this.state.submittingRequest ?
                          <p><a onClick={this.requestContactInfo.bind(this)}>Request performer contact information</a></p>
                        :
                          <p>Submitting request...</p>
                      }
                    </li>
                  :
                    <li>
                      <p>Performer has no listed contact information</p>
                    </li>
              }
              <li>
                {
                    this.state.isEdit ?
                    <p>This performer was rated <EditableField
                            obj={this.state.obj}
                            label="performer_effectiveness"
                            type="select"
                            options={[
                              "0",
                              "1",
                              "2",
                              "3",
                              "4",
                              "5"
                            ]}
                            changes={this.state.changes}
                            setState={this.setState.bind(this)}
                            editing={this.state.isEdit}
                            isAdmin={this.state.isAdmin}
                          /> /5</p>
                  : this.state.obj.performer_effectiveness != 0 ?
                    <p>This performer was <span className = "undl">{this.quality[this.state.obj.performer_effectiveness]}</span> ({this.state.obj.performer_effectiveness}/5)</p>
                  : <p>This performers effectiveness is unknown</p>
                }
              </li>
                  <li><p>This performer was a <EditableField
                                                  obj={this.state.obj}
                                                  label="performer_student_guest"
                                                  type="select"
                                                  options={[
                                                    "Student",
                                                    "Guest"
                                                  ]}
                                                  displayFilter={
                                                    (val) => {
                                                      if (typeof val == "string" && val != "")
                                                        return <span className="emphasis">{val.toLowerCase()}</span>
                                                      else
                                                        return <span className="emphasis">unknown (student or guest)</span>
                                                    }
                                                  }
                                                  changes={this.state.changes}
                                                  setState={this.setState.bind(this)}
                                                  editing={this.state.isEdit}
                                                  isAdmin={this.state.isAdmin}
                                                />
                  </p></li>
              <li>
                  <p>This performers specialy is <EditableField
                                                      obj={this.state.obj}
                                                      label="performer_specialty"
                                                      type="text"
                                                      displayFilter={
                                                        (val) => {
                                                          if (val != "") {
                                                            return <span className="emphasis">{val}</span>
                                                          } else {
                                                            return <span>unlisted</span>
                                                          }
                                                        }
                                                      }
                                                      changes={this.state.changes}
                                                      setState={this.setState.bind(this)}
                                                      editing={this.state.isEdit}
                                                      isAdmin={this.state.isAdmin}
                                                    />
                  </p>
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing= {this.state.obj.picture_url != null && this.state.obj.picture_url != ''}
            obj = {this.state.obj}
            headerElements = {[]}
            title = "Picture(s)"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            {
              this.state.isEdit ?
                <p>Picture urls (separate by commas): <EditableField
                                    obj={this.state.obj}
                                    label="picture_url"
                                    type="text"
                                    changes={this.state.changes}
                                    setState={this.setState.bind(this)}
                                    editing={this.state.isEdit}
                                    isAdmin={this.state.isAdmin}
                                  /></p>
              : this.state.obj.picture_url != null ?
                <Gallery photos={photos} onClick={this.openLightbox.bind(this)} />
              : ""
            }
          </BasicModule>

          <Lightbox images={photos}
            onClose={this.closeLightbox.bind(this)}
            onClickPrev={this.gotoPrevious.bind(this)}
            onClickNext={this.gotoNext.bind(this)}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />

          <SkyLight
            ref={ref => this.requestEditModal = ref}
            dialogStyles = {
              {
                marginTop:0,
                top:"10%",
                height:"80%",
                overflow:"auto"
              }
            }
            hideOnOverlayClicked
          >

            <label>What changes need to be made?</label>
            <textarea onChange={this.textFieldChange.bind(this)} className="mdl-textfield__input" type="text" rows= "8" col="50"></textarea>
            <br/>
            <button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.submitSuggestedEdit.bind(this)}>
              Submit
            </button>
            <button className="mdl-button mdl-js-button mdl-button--raised" onClick={() => this.requestEditModal.hide()}>
              Cancel
            </button>

          </SkyLight>

          <SkyLight
            ref={ref => this.modal = ref}
            hideOnOverlayClicked
            dialogStyles = {
              {
                height:"auto"
              }
            }>

            <h3>{this.state.requestmessage}</h3>

          </SkyLight>
        </div>
      )
    } else {
      return (
        <div>
          <Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">
            <span className="mdl-layout-title mdl-cell mdl-cell--12-col">Rally not found!</span>
          </Card>
        </div>
      )
    }
  }
}
