let getCurrentYearOfMinistry = () => {
	const current_date = new Date();

	let current_month = current_date.getMonth();

	let max_date;
	let min_date;

	if (current_month < 6) {
		min_date = `${current_date.getFullYear() - 1}-07-01`;
		max_date = `${current_date.getFullYear()}-06-30`;
	}
	else {
		min_date = `${current_date.getFullYear()}-07-01`;
		max_date = `${current_date.getFullYear() + 1}-06-30`;
	}

	return {
		max_date,
		min_date
	};
}

export {
	getCurrentYearOfMinistry
}
