import {
	getPermissions,
	returnSQLResponse
} from '../util';
import moment from 'moment';

export const getRallies = (request, response, connection) => {
	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT rallies.id,rallies.timestamp,rallies.card_explained,rallies.card_explainedby,rallies.club_attendance,rallies.collected_data,rallies.was_contact_cards,rallies.daily_theme,rallies.date,rallies.stories,rallies.indicated_decisions,rallies.location,rallies.lunch_periods,rallies.materials_bibles,rallies.was_materials_distributed,rallies.materials_gospel_booklets,rallies.materials_gospels_of_john,rallies.materials_lifebooks,rallies.materials_other,rallies.materials_other_type,rallies.was_materials_table,rallies.performer_name,rallies.performer_organization,rallies.performer_specialty,rallies.performer_stagename,rallies.performer_student_guest,rallies.was_performer,rallies.performer_effectiveness,rallies.speaker_engagement,rallies.speaker_explained_saved,rallies.speaker_gospel_clear,rallies.speaker_gospel_shared,rallies.speaker_invitation,rallies.speaker_isperformer,rallies.speaker_name,rallies.speaker_organization,rallies.speaker_physical_invitation,rallies.speaker_student_guest,rallies.speaker_topic,rallies.speaker_topically_known,rallies.was_speaker,rallies.student_names_participating,rallies.students_participating,rallies.time,rallies.type,rallies.week_theme,rallies.total_attendance,rallies.speaker_explained_saved_clear,rallies.schoolreg_id,rallies.picture_url, ((speaker_email IS NULL OR TRIM(speaker_email) = '') AND (speaker_phone IS NULL OR TRIM(speaker_phone) ='')) AS 'speaker_no_contact', ((performer_email IS NULL OR TRIM(performer_email) = '') AND (performer_phone IS NULL OR TRIM(performer_phone) = '')) AS 'performer_no_contact',schools.name AS 'school_name', schools.id AS 'school_id' ` +
		`FROM rallies JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id JOIN schools ON school_registration.school_id = schools.id
		WHERE
		school_registration.chapter_id IN ${chaptersSQL}
		OR
		school_registration.school_id IN ${schoolsSQL}
		OR
		${hasFullAccess}
		ORDER BY date DESC`
		,
		returnSQLResponse(connection, response)
	);
};

export const getAllRaillesByChapterIds = (request, response, connection) => {
	const {ids} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT
			rallies.date,
			rallies.stories,
			rallies.id,
			rallies.total_attendance,
			rallies.indicated_decisions,
			rallies.schoolreg_id,
			rallies.daily_theme,
			rallies.speaker_gospel_shared,
			schools.name as 'school_name',
			schools.id as 'school_id'
		FROM ((rallies
			INNER JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id)
			INNER JOIN schools ON school_registration.school_id = schools.id)
		WHERE school_registration.chapter_id IN(${ids})
		AND (
			school_registration.chapter_id IN ${chaptersSQL}
			OR
			school_registration.school_id IN ${schoolsSQL}
			OR
			${hasFullAccess}
		)
		${startDate ? `AND date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
		${endDate ? `AND date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}`,
		returnSQLResponse(connection, response)
	);
};

export const getRalliesByChapterId = (request, response, connection) => {
	const {end_date, id, start_date} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT rallies.id,rallies.timestamp,rallies.card_explained,rallies.card_explainedby,rallies.club_attendance,rallies.collected_data,rallies.was_contact_cards,rallies.daily_theme,rallies.date,rallies.stories,rallies.indicated_decisions,rallies.location,rallies.lunch_periods,rallies.materials_bibles,rallies.was_materials_distributed,rallies.materials_gospel_booklets,rallies.materials_gospels_of_john,rallies.materials_lifebooks,rallies.materials_other,rallies.materials_other_type,rallies.was_materials_table,rallies.performer_name,rallies.performer_organization,rallies.performer_specialty,rallies.performer_stagename,rallies.performer_student_guest,rallies.was_performer,rallies.performer_effectiveness,rallies.speaker_engagement,rallies.speaker_explained_saved,rallies.speaker_gospel_clear,rallies.speaker_gospel_shared,rallies.speaker_invitation,rallies.speaker_isperformer,rallies.speaker_name,rallies.speaker_organization,rallies.speaker_physical_invitation,rallies.speaker_student_guest,rallies.speaker_topic,rallies.speaker_topically_known,rallies.was_speaker,rallies.student_names_participating,rallies.students_participating,rallies.time,rallies.type,rallies.week_theme,rallies.total_attendance,rallies.speaker_explained_saved_clear,rallies.schoolreg_id,rallies.picture_url, ((speaker_email IS NULL OR TRIM(speaker_email) = '') AND (speaker_phone IS NULL OR TRIM(speaker_phone) ='')) AS 'speaker_no_contact', ((performer_email IS NULL OR TRIM(performer_email) = '') AND (performer_phone IS NULL OR TRIM(performer_phone) = '')) AS 'performer_no_contact'` +
		` FROM rallies WHERE schoolreg_id	IN(SELECT school_registration.registration_id FROM school_registration
			WHERE school_registration.chapter_id=${id})AND date >= '${start_date}' AND date <='${end_date}
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)'
			ORDER BY date ASC
			${startDate ? `AND date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
			${endDate ? `AND date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}`,

		returnSQLResponse(connection, response)
	);
};

export const getRalliesBySchoolId = (request, response, connection) => {
	const {id} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT
			rallies.date,
			rallies.stories,
			rallies.id,
			rallies.total_attendance,
			rallies.indicated_decisions,
			rallies.schoolreg_id,
			rallies.daily_theme,
			rallies.speaker_gospel_shared,
			schools.name as 'school_name',
			schools.id as 'school_id'
		FROM ((rallies
			INNER JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id)
			INNER JOIN schools ON school_registration.school_id = schools.id)
		WHERE schools.id IN(${id})
		AND (
			school_registration.chapter_id IN ${chaptersSQL}
			OR
			school_registration.school_id IN ${schoolsSQL}
			OR
      		${hasFullAccess}
		)
		${startDate ? `AND date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
		${endDate ? `AND date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}`,
		returnSQLResponse(connection, response)
	);
};

export const getLiteRalliesBySchoolId = (request,response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT
			rallies.date,
			rallies.id
		FROM ((rallies
			INNER JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id)
			INNER JOIN schools ON school_registration.school_id = schools.id)
		WHERE schools.id IN(${id}) ORDER BY rallies.date
		AND (
			school_registration.chapter_id IN ${chaptersSQL}
			OR
			school_registration.school_id IN ${schoolsSQL}
			OR
			${hasFullAccess}
		)`,
		returnSQLResponse(connection, response)
	);
}

export const getRally = (request, response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT rallies.id,rallies.timestamp,rallies.card_explained,rallies.card_explainedby,rallies.club_attendance,rallies.collected_data,rallies.was_contact_cards,rallies.daily_theme,rallies.date,rallies.stories,rallies.indicated_decisions,rallies.location,rallies.lunch_periods,rallies.materials_bibles,rallies.was_materials_distributed,rallies.materials_gospel_booklets,rallies.materials_gospels_of_john,rallies.materials_lifebooks,rallies.materials_other,rallies.materials_other_type,rallies.was_materials_table,rallies.performer_name,rallies.performer_organization,rallies.performer_specialty,rallies.performer_stagename,rallies.performer_student_guest,rallies.was_performer,rallies.performer_effectiveness,rallies.speaker_engagement,rallies.speaker_explained_saved,rallies.speaker_gospel_clear,rallies.speaker_gospel_shared,rallies.speaker_invitation,rallies.speaker_isperformer,rallies.speaker_name,rallies.speaker_organization,rallies.speaker_physical_invitation,rallies.speaker_student_guest,rallies.speaker_topic,rallies.speaker_topically_known,rallies.was_speaker,rallies.student_names_participating,rallies.students_participating,rallies.time,rallies.type,rallies.week_theme,rallies.total_attendance,rallies.speaker_explained_saved_clear,rallies.schoolreg_id,rallies.picture_url, ((speaker_email IS NULL OR TRIM(speaker_email) = '') AND (speaker_phone IS NULL OR TRIM(speaker_phone) ='')) AS 'speaker_no_contact', ((performer_email IS NULL OR TRIM(performer_email) = '') AND (performer_phone IS NULL OR TRIM(performer_phone) = '')) AS 'performer_no_contact',schools.name AS 'school_name',chapters.name AS 'chapter_name',schools.id AS 'school_id',chapters.id AS 'chapter_id' FROM rallies JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id JOIN schools ON school_registration.school_id = schools.id JOIN chapters ON school_registration.chapter_id = chapters.id ` +
		`WHERE rallies.id = ${id}
		AND (
			school_registration.chapter_id IN ${chaptersSQL}
      OR
      school_registration.school_id IN ${schoolsSQL}
      OR
      ${hasFullAccess}
		)`,
		returnSQLResponse(connection, response)
	);
};
