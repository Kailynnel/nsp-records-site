import {
	withDBConnection,
	withAdminDBConnection,
	isLoggedIn
} from '../util';
import {
	getSchool,
	getSchools,
	getSchoolsByChapterId,
	getSchoolsNameAndIdFromSchoolRegId,
	getUniqueSchoolIdsFromChapterIds,
	getRegistrationInfoForSchool,
	getSchoolsForDate
} from './SchoolsController';

export default app => {
	app.route('/api/school_reg/:id')
		.get(isLoggedIn, withDBConnection(getRegistrationInfoForSchool))

	app.route('/api/schools')
		.get(isLoggedIn, withDBConnection(getSchools));

	app.route('/api/schools/:id')
		.get(isLoggedIn, withDBConnection(getSchool));

	app.route('/api/schools/name/id/school_reg_ids/:ids')
		.get(isLoggedIn, withDBConnection(getSchoolsNameAndIdFromSchoolRegId));

	app.route('/api/schools/chapter_ids/:ids')
		.get(isLoggedIn, withDBConnection(getSchoolsByChapterId));

	app.route('/api/schools/school_id/distinct/chapter_ids/:ids')
		.get(isLoggedIn, withDBConnection(getUniqueSchoolIdsFromChapterIds));

	app.route('/api/admin/getschools/:date')
		.get(isLoggedIn, withAdminDBConnection(getSchoolsForDate));
}